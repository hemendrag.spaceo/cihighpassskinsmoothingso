//
//  CIHighPassSkinSmoothing.h
//  CIHighPassSkinSmoothing
//
//  Created by Alexander Shinkarenko on 16/05/2019.
//  Copyright © 2019 Alexander Shinkarenko. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CIHighPassSkinSmoothing.
FOUNDATION_EXPORT double CIHighPassSkinSmoothingVersionNumber;

//! Project version string for CIHighPassSkinSmoothing.
FOUNDATION_EXPORT const unsigned char CIHighPassSkinSmoothingVersionString[];

#import <CIHighPassSkinSmoothing/YUCIHighPassSkinSmoothing.h>
#import <CIHighPassSkinSmoothing/YUCIHighPass.h>

// In this header, you should import all the public headers of your framework using statements like #import <CIHighPassSkinSmoothing/PublicHeader.h>


